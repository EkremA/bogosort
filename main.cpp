/******************************************************************************
Deterministic Hardware Demo Bogosort Demo 
*******************************************************************************/
#include <iostream>
#include <algorithm>    //std::shuffle
#include <vector>       //std::vector<T>
#include <random>
#include <chrono>       //system_clock

bool isSame(std::vector<int> arr1, std::vector<int> arr2){
    if(arr1.size()!=arr2.size()){
        std::cout << "The arrays in question do not have the same length! Error";
    };
    for(int i=0; i<arr1.size(); i++){
        if (arr1[i] != arr2[i]){
          return false;  
        };
    };
    return true;  
};

/*template <typename T>
void shuffleWrapper(std::vector<T> arr){
    std::random_shuffle(arr.begin(),arr.end(),randdev);
};
*/
int main()
{
    int numEl = 3;
    std::vector<int> original;
    std::vector<int> copy;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine randeng (seed);  //time seed
    
    //generate list randomly
    //make a copy
    for(int i=0; i<numEl; i++){
        original.push_back(randeng());
    };
    copy = original;
    //start unordered
    while(isSame(copy,original)){
        std::shuffle(copy.begin(),copy.end(),randeng); 
    };
    
    //Bogosort
    bool isSuccess;
    int numIter = 0;
    std::cout << "Starting Bogosort..." << std::endl;
    while(!isSuccess){
        std::shuffle(copy.begin(),copy.end(),randeng);
        numIter++;
        isSuccess = isSame(copy, original);
        std::cout << "Iteration " << numIter << " finished." << std::endl;
    };
    
    std::cout << "Sorting success! Number of iterations: " << numIter << " (time needed: " << "-tbd-)" << std::endl;
    //TODO: Timer for time needed to sort 
    //TODO: verbose mode with which output at every sorting iteration is enabled/disabled 
    
    
    
    
    //std::cout << isSame(copy, original) << std::endl;
    /*
    for(int i=0; i<numEl; i++){
        std::cout << "original[" << i << "]: " << original[i] << std::endl;
        std::cout << "copy[" << i << "]:     " << copy[i] << std::endl;
        std::cout << "=========================================" << std::endl;
        
    };
    */
    return 0;
}


